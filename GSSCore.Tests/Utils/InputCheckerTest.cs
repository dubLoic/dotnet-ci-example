﻿using GoStyleServiceCore.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace GSSCoreTests.Utils
{

    [TestClass]
    public class InputCheckerTest
    {
        [TestMethod]
        public void WrongIfUsername_length_13()
        {
            string input = "TestUsername1";
            bool result = input.CheckUsername();

            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void WrongIfUsername_length_4()
        {
            string input = "Test";
            bool result = input.CheckUsername();

            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void TrueIfUsername_length_10()
        {
            string input = "TestUserna";
            bool result = input.CheckUsername();

            Assert.AreEqual(true, result);
        }
    }
}
