﻿using GoStyleServiceCore.DTO;
using GoStyleServiceCore.Services;
using GoStyleServiceCore.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoStyleServiceCore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private IUserService _userService;

        public UserController(ILogger<UserController> logger)
        {
            _logger = logger;
            _userService = new UserService();
        }

        [HttpPost]
        [Route("create")]
        public AuthenticatedUser Post([FromBody] RegisterUser user)
        {
            if (user.IsValid())
            {
                _userService.Create(new Entities.User(user), out bool success);
                return new AuthenticatedUser
                {
                    User = user != null ? new UserDTO(user) : new UserDTO(),
                    Success = success,
                    Message = success ? "Success" : "Failed"
                };
            }
            else
            {
                return new AuthenticatedUser
                {
                    User = null,
                    Success = false,
                    Message = "Inputs failed"
                };
            }
        }

        [Route("authenticate")]
        public AuthenticatedUser Post([FromBody] AuthenticateUser user)
        {
            return new AuthenticatedUser
            {
                User = new UserDTO(_userService.Authenticate(user.Username, user.Password.ToSha256(), out bool success)),
                Success = success,
                Message = success ? "Success" : "Failed"
            };
        }

        [HttpGet]
        public string Get()
        {
            return "Hello";
        }
    }
}
