﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoStyleServiceCore.DTO
{
    public class AuthenticateUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}