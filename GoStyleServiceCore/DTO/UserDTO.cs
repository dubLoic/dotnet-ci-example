﻿using GoStyleServiceCore.Entities;
using GoStyleServiceCore.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoStyleServiceCore.DTO
{
    public class UserDTO
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }

        public UserDTO()
        {

        }
        public UserDTO(User user)
        {
            if(user != null)
            {
                Firstname = user.firstname;
                Lastname = user.lastname;
                Username = user.username;
            }
        }
        public UserDTO(RegisterUser user)
        {
            if (user != null)
            {
                Firstname = user.Firstname;
                Lastname = user.Lastname;
                Username = user.Username;
            }
        }
    }
}