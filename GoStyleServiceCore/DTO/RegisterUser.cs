﻿using GoStyleServiceCore.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoStyleServiceCore.DTO
{
    public class RegisterUser
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public bool IsValid()
        {
            bool result = true;
            if (!Firstname.CheckFirstname()) 
                result = false;
            if (!Lastname.CheckLastname()) 
                result = false;
            if (!Username.CheckUsername()) 
                result = false;
            if (!Password.CheckPassword()) 
                result = false;
            return result;
        }
    }
}