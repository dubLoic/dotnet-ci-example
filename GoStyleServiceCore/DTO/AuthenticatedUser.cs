﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoStyleServiceCore.DTO
{
    public class AuthenticatedUser
    {
        public UserDTO User { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
    }
}