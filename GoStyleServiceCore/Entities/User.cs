﻿using GoStyleServiceCore.DTO;
using GoStyleServiceCore.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoStyleServiceCore.Entities
{
    public class User
    {
        public int id { get; set; }
        public DateTime createdOn { get; set; }
        public string username { get; set; }
        public string shaPassword { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }

        public User() { }
        public User(RegisterUser user)
        {
            username = user.Username;
            firstname = user.Firstname;
            lastname = user.Lastname;
            shaPassword = user.Password.ToSha256();
        }
    }
}