﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace GoStyleServiceCore.Utils
{
    public static class InputChecker
    {
        public static bool CheckUsername(this string str)
        {
            if (str.Length < 5 || str.Length > 12)
                return false;
            else if (Regex.IsMatch(str, @"^[A-Za-z0-9_@./#&+-]+$"))
                return true;
            else 
                return false;
        }

        public static bool CheckFirstname(this string str)
        {
            if (str.Length < 2 || str.Length > 20)
                return false;
            else if (Regex.IsMatch(str, @"^[a-zA-Z- ]+$"))
                return true;
            else
                return false;
        }

        public static bool CheckLastname(this string str)
        {
            if (str.Length < 2 || str.Length > 20)
                return false;
            else if (Regex.IsMatch(str, @"^[a-zA-Z- ]+$"))
                return true;
            else
                return false;
        }

        public static bool CheckPassword(this string str)
        {
            if (str.Length < 6 || str.Length > 16)
                return false;
            else if (Regex.IsMatch(str, @"^[A-Za-z0-9_@./#&+-]+$"))
                return true;
            else
                return false;
        }


        public static string ToSha256(this string str)
        {
            return str;
        }
        public static string FromSha256(this string str)
        {
            return str;
        }
    }
}