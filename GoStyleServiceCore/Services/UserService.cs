﻿using Dapper;
using GoStyleServiceCore.DataAccess;
using GoStyleServiceCore.Entities;
using GoStyleServiceCore.Services;
using GoStyleServiceCore.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GoStyleServiceCore.Services
{
    public class UserService : IUserService
    {
        public User Authenticate(string username, string shaPassword, out bool success)
        {
            using IDbConnection cnn = new SqlConnection(SQLDataAccess.GetConnectionString());
            var p = new DynamicParameters();
            p.Add("@Username", username);
            p.Add("@Password", shaPassword);
            string sql = @"exec dbo.connect @Username, @Password";
            User data = cnn.Query<User>(sql, p).FirstOrDefault();
            success = data != null;
            return data ?? null;
        }

        public void Create(User u, out bool success)
        {
            success = false;
            using (IDbConnection cnn = new SqlConnection(SQLDataAccess.GetConnectionString()))
            {
                var p = new DynamicParameters();
                p.Add("@Username", u.username);
                p.Add("@Firstname", u.firstname);
                p.Add("@Lastname", u.lastname);
                p.Add("@Password", u.shaPassword);

                string sql = $@"exec dbo.register @Username, @Password, @Firstname, @Lastname";

                cnn.Open();
                using (var trans = cnn.BeginTransaction())
                {
                    int recordsUpdated = cnn.Execute(sql, p, trans);

                    try
                    {
                        trans.Commit();
                        success = recordsUpdated > 0; 
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error: {ex.Message}");
                        trans.Rollback();
                    }
                }
            }
        }
    }
}