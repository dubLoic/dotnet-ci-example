﻿using GoStyleServiceCore.DTO;
using GoStyleServiceCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoStyleServiceCore.Services
{
    interface IUserService
    {
        void Create(User u, out bool success);
        User Authenticate(string username, string shaPassword, out bool success);
    }
}
